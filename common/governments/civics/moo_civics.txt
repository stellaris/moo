# MOO: Master of Orion Universe
# Author: Iyur
# Custom civics

# Any, replaces dlc one
civic_moo_diplomatic_corps = {

	icon = "gfx/interface/icons/governments/civics/civic_diplomatic_corps.dds"

	playable = { not = { host_has_dlc = "Federations" } }
	potential = {
		ethics = { NOT = { value = ethic_gestalt_consciousness } }
		authority = { NOT = { value = auth_corporate } }
	}
	possible = {
		civics = {
			NOR = {
				value = civic_fanatic_purifiers
				value = civic_inwards_perfection
			}
		}
	}

	modifier = {
		envoys_add = 2
		diplo_weight_mult = 0.1
	}

}

# Darlok
civic_moo_spy_network = {

	icon = "gfx/interface/icons/governments/civics/civic_empire_in_decline.dds"

	potential = {
		ethics = { NOT = { value = ethic_gestalt_consciousness } }
		authority = { NOT = { value = auth_corporate } }
	}
	possible = {
		ethics = {
			or = {
				value = ethic_xenophobe
				value = ethic_fanatic_xenophobe				
			}
		}	
	}

	modifier = {
		diplo_weight_mult = 0.1
		planet_stability_add = 5
	}
		
}

# Klackon
civic_moo_swarm = {

	icon = "gfx/interface/icons/governments/civics/civic_ancient_caches_of_technology.dds"
	description = "civic_moo_swarm_effect"
	modification = no

	potential = {
		ethics = { NOT = { value = ethic_gestalt_consciousness } }
	}

	possible = {
		ethics = {
			or = {
				value = ethic_authoritarian
				value = ethic_fanatic_authoritarian				
			}
		}		
	}

	traits = {
		trait = trait_hive_mind
	}

	modifier = {
		pop_growth_speed = 0.15
		pop_amenities_usage_mult = -0.15
	}

	random_weight = { base = 0 }
}

# Psilon
civic_moo_creative_researchers = {

	icon = "gfx/interface/icons/governments/civics/civic_machine_remnants.dds"

	potential = {
		ethics = { NOT = { value = ethic_gestalt_consciousness } }
		authority = { NOT = { value = auth_corporate } }
	}
	possible = {
		ethics = {
			value = ethic_fanatic_materialist
		}
		civics = {
			value = civic_technocracy
		}		
	}

	modifier = {
		num_tech_alternatives_add = 1
	}
		
}

# Elerian
civic_moo_telepathic_training = {

	icon = "gfx/interface/icons/governments/civics/civic_final_defense_protocols.dds"
	description = "civic_moo_telepathic_training_effect"
	modification = no

	potential = {
		ethics = { NOT = { value = ethic_gestalt_consciousness } }
		authority = { NOT = { value = auth_corporate } }
	}

	possible = {
		ethics = {
			or = {
				value = ethic_spiritualist
				value = ethic_fanatic_spiritualist				
			}
		}
		civics = {
			not = { value = civic_technocracy }
		}		
	}

	traits = {
		trait = trait_moo_telepathic
	}

	modifier = {
		diplo_weight_mult = 0.10
		category_psionics_research_speed_mult = 0.15
	}

	random_weight = { base = 0 }
}